import socket
import json
import os
import db_api

HOST = 'x.x.x.x'
PORT = 12345
BUFSIZ = 1024
DB_FILE = "statistics.db"

def send_to_server_tcp(packet_list,p_type):
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = HOST #input("Enter hostname [%s]: " %HOST) or HOST
    port = PORT #input("Enter port [%s]: " %PORT) or PORT

    sock_addr = (host, int(port))
    client_sock.connect(sock_addr)
    try:
        for row in packet_list:
            row = list(row)
            row.insert(0, p_type)
            client_sock.send( json.dumps(row)) #payload.encode('utf-8'))
            data = client_sock.recv(BUFSIZ)
            print(repr(data))
			
    except Exception as e:
        print(e) 

    client_sock.close()
	
if __name__ == '__main__':
    #Create Database if not exists
    if os.path.isfile(DB_FILE):  
        print "Database exists."
    else:
        print "Creating DataBase..."
        db_api.parse_vendors()
        conn = db_api.database_init()
        print "Populating Vendors table..."
        db_api.populate_vendors_table(conn)    

    conn = db_api.create_connection(DB_FILE)
    ################## Data to be send to server ############################
    data = db_api.db_fetch_packet_data(conn)
    send_to_server_tcp(data,0)
    data = db_api.db_fetch_packet_data(conn,'prob_packet')
    send_to_server_tcp(data,1)
    ################## This must be done periodically#########################
    sessions = db_api.db_get_sessions(conn)
    
    print ' Number of sessions: ',len(sessions)
    if len(sessions) == 0:
        print "No sessions inserted to database."
    else:
        print "\n Inserting sessions to database..."
        db_api.db_insert_sessions(conn,sessions)
        print "Done. Now we can remove old data packet entries."
    # If everything goes well, we can delete from local db the old data_packets
    print "Removing old entries..." 
    db_api.remove_old_entries(conn,'data_packet')
    db_api.remove_old_entries(conn,'prob_packet')

