import sys
import socket
import os
from time import ctime
import json
import server_db

HOST = 'x.x.x.x' #'localhost'
PORT = 12345
BUFSIZ = 1024
ADDR = (HOST, PORT)
DB_FILE = "statistics_server.db"

if __name__ == '__main__':

    if os.path.isfile(DB_FILE):  
        print "Database exists"
    else:
        print "Creating DataBase..."
        server_db.database_init()
 
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(ADDR)
    server_socket.listen(5)
    server_socket.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )
    
    while True:
        data_packets = []
        prob_packets = []
        print('Server waiting for connection...')
        client_sock, addr = server_socket.accept()
        print('Client connected from: ', addr)
        
        while True:
            data = client_sock.recv(BUFSIZ)
            try:
                data = json.loads(data)
                if data[0] == 0:
                    data_packets.append(data)
                elif data[0] == 1:
                    prob_packets.append(data)
            except Exception as e:
                print e
                #data = data.decode('utf- 8')

            if not data: #or data.decode('utf-8') == 'END':
                break
            #print("Received from client: %s" % data) #data.decode('utf- 8'))
            #print("Sending the server time to client: %s"  %ctime())
            try:
                client_sock.send(bytes(ctime()))
            except KeyboardInterrupt:
                print("Exited by user")
        
        conn = server_db.create_connection(DB_FILE)
        print "inserting data to Database..."
        if len(data_packets) != 0:
            server_db.db_insert_datap(conn, data_packets)
        elif len(prob_packets) != 0:
            server_db.db_insert_probp(conn, prob_packets)
        print "\nOK"
        client_sock.close()
    server_socket.close()


