# Wifi Monitoring

a tool for monitoring wifi usage in urban areas. It creates a local database "statistics.db" using sqlite3 where it stores
the captured packets.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine or router for development and
testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

You need to install python2.7 and scapy v2+ although it is possible that the tool will work fine with older scapy versions.

On openwrt:
```
 opkg install python
 opkg install scapy
```
### Get the system running

 Clone repo
```
git clone http://repo.nitlab.inf.uth.gr/niksak/wifi-monitoring.git
```

Clone the patched itamae repo inside the root directory of wifi-monitoring.
```
https://github.com/niksak/itamae.git
```

Run(optionaly) db_api.py to initialize the "statistics.db" sqlite3 database. It should run only once.
If it is not run manually, monitor.py will initialize it when run for the first time.
```
 python db_api.py
```
Set your router to monitor mode.
```
iw del mon0
iw phy phy0 interface add mon0 type monitor
ifconfig mon0 up
```

The main srcipt that periodicaly monitors wifi traffic is monitor.py.
When the script stops it will save all monitored traffic to "statistics.db"
```
 python monitor.py
```
Add a cronjob for running it periodically.

```
 https://wiki.openwrt.org/doc/howto/cron
```

If you need to send the data to a remote server, copy the tcp_server.py and server_db.py to your remote server. 
Change the bind method inside the script to the address of your server.
Edit the tcp_client.py and change the host to your remote server address.

vim tcp_server.py
```
HOST = 'server_ip' 
PORT = 12345
```

vim tcp_client.py
```
HOST = 'your_remote_server_ip'
PORT = 12345
```

On the remote server run server_db.py to initialize server's database.
```
  python server_db.py
```
and start the server:
```
  python tcp_server.py
```

Finally add to the router's crontab the client.py for running whenever you want to clear the local database and send the data to remote server.
Keep in mind that as spartially the job is executed, the local database will get bigger depending on how frequently monitor.py is running.

or just run tcp_server.py on the remote server and auto_script.py on the rooter. The auto_script.py will run the monitor code and send the statistics to
the remote server indefinetely. 

 
