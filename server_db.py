import sqlite3

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except: #Error as e:
        print("Error trying to connect")
 
    return None

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    with conn:
        c = conn.cursor()
        c.execute(create_table_sql)
		
def database_init():
	"""
	Description : Create database "statistics.db" and create vendors,
		      association_packet and data_packet tables. Needs to run one time. 
	Input       : 
	Output      : 
	"""
	database = "statistics_server.db"
 
	sql_create_vendors_table = """ CREATE TABLE IF NOT EXISTS vendors (
                                        id integer PRIMARY KEY,
                                        vendor_id text NOT NULL,
                                        vendor_name text,
                                        mac_address text
                                    ); """
 
	sql_create_assocpacket_table = """ CREATE TABLE IF NOT EXISTS association_packet (
				    				id integer PRIMARY KEY,
                                    mac text NOT NULL,
									capabilities text NOT NULL,
                                    ap_to_connect text NOT NULL,rssi integer,timestamp integer
                                    
                                );"""

	sql_create_data_packet_table = """ CREATE TABLE IF NOT EXISTS data_packet (id integer PRIMARY KEY,
										mac text NOT NULL,
										destination text,
										payload_length integer,
										rate integer,rssi integer,vendor text,
										timestamp integer);""" 
	sql_create_beacons_packet_table =  """ CREATE TABLE IF NOT EXISTS beacon_packet (
										id integer PRIMARY KEY,
										mac text NOT NULL,
										capabilities text NOT NULL,
										vendor text,channel integer,rssi integer,
										timestamp integer); """


	sql_create_probr_packet_table =  """ CREATE TABLE IF NOT EXISTS prob_packet (
										id integer PRIMARY KEY,
										mac text NOT NULL,
										destination text NOT NULL,
										rate integer,
										vendor text,channel integer,rssi integer,
										timestamp integer); """

	#FOREIGN KEY (project_id) REFERENCES projects (id)
	# create a database connection
	conn = create_connection(database)
	if conn is not None:
        # create projects table
		create_table(conn, sql_create_vendors_table)
        # create tasks table
		create_table(conn, sql_create_assocpacket_table)
		create_table(conn,sql_create_data_packet_table)
		create_table(conn,sql_create_beacons_packet_table)
		create_table(conn,sql_create_probr_packet_table)
	else:
		print("Error! cannot create the database connection.")
	return conn

def db_insert_datap(conn, datap_list):

    insert_datapacket_table = """ INSERT INTO data_packet(mac,destination,payload_length,rate,rssi,vendor,timestamp) VALUES (?,?,?,?,?,?,?);"""
    with conn:
        #vendors = fetch_vendors(conn)
        c = conn.cursor()
        for row in datap_list:
            mac = row[1]
            destination = row[2]
            payload_length = row[3]
            rate = row[4]
            rssi = row[5]
            timestamp = row[7]
            vendor = row[6]
            #c.execute(insert_datapacket_table,(mac,destination,payload_length,rate,rssi,vendor,timestamp))
			#check if packet already exists in data_packet table
            #c.execute('SELECT EXISTS(SELECT 1 FROM data_packet WHERE timestamp=? LIMIT 1);',(timestamp,))
            #exists = c.fetchone()
            #if exists[0] == 0:			
            c.execute(insert_datapacket_table,(mac,destination,payload_length,rate,rssi,vendor,timestamp))

def db_insert_datap(conn, datap_list):

    insert_datapacket_table = """ INSERT INTO data_packet(mac,destination,payload_length,rate,rssi,vendor,timestamp) VALUES (?,?,?,?,?,?,?);"""
    with conn:
        #vendors = fetch_vendors(conn)
        c = conn.cursor()
        for row in datap_list:
            mac = row[2]
            destination = row[3]
            payload_length = row[4]
            rate = row[5]
            rssi = row[6]
            timestamp = row[8]
            vendor = row[7]
            #c.execute(insert_datapacket_table,(mac,destination,payload_length,rate,rssi,vendor,timestamp))
			#check if packet already exists in data_packet table
            #c.execute('SELECT EXISTS(SELECT 1 FROM data_packet WHERE timestamp=? LIMIT 1);',(timestamp,))
            #exists = c.fetchone()
            #if exists[0] == 0:			
            c.execute(insert_datapacket_table,(mac,destination,payload_length,rate,rssi,vendor,timestamp))

def db_insert_probp(conn, datap_list):

    insert_probpacket_table = """ INSERT INTO prob_packet(mac,destination,rate,rssi,vendor,channel,timestamp) VALUES (?,?,?,?,?,?,?);"""
    with conn:
        #vendors = fetch_vendors(conn)
        c = conn.cursor()
        for row in datap_list:
            mac = row[2]
            destination = row[3]
            
            rate = row[4]
            rssi = row[5]
            timestamp = row[8]
            vendor = row[6]
            channel = row[7]
            #c.execute(insert_datapacket_table,(mac,destination,payload_length,rate,rssi,vendor,timestamp))
			#check if packet already exists in data_packet table
            #c.execute('SELECT EXISTS(SELECT 1 FROM data_packet WHERE timestamp=? LIMIT 1);',(timestamp,))
            #exists = c.fetchone()
            #if exists[0] == 0:			
            c.execute(insert_probpacket_table,(mac,destination,rate,rssi,vendor,channel,timestamp))
   
if __name__ == '__main__':
    
	#answer = raw_input("Are you sure you want to initialize the database?")
    print "Creating database... "
    conn = database_init()
    print "Database is ready"
	
