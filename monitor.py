
#!/usr/bin/env python

import sys
from scapy.all import *
import datetime
import db_api
import time
import subprocess
#import itamae.radiotap as rtap

sys.path.insert(0,'/root/monitor/wifi-monitoring/nsak_itamae/itamae')
import itamae.radiotap as rtap

ap_list = []
data_packet = {}
beacon_packet = {}
assoc_packet = {}
prob_packet = {}
'''
Name       : packet_info
Description: Retrieve info from packet.
Input      : Packet
Output     : channel
'''
def packet_info(pkt):
    bssid = pkt[Dot11].addr3
    p = pkt[Dot11Elt]
    cap = pkt.sprintf("{Dot11Beacon:%Dot11Beacon.cap%}"
                      "{Dot11ProbeResp:%Dot11ProbeResp.cap%}").split('+')
    ssid, channel, rate = None, None, None
    crypto = set()
    while isinstance(p, Dot11Elt):
        if p.ID == 0:
            ssid = p.info
	elif p.ID == 1: #221 : vendor
	    rate = p.info
	elif p.ID == 3:
            channel = ord(p.info)
        elif p.ID == 48:
            crypto.add("WPA2")
        elif p.ID == 221 and p.info.startswith('\x00P\xf2\x01\x01\x00'):
            crypto.add("WPA")
        p = p.payload
    if not crypto:
        if 'privacy' in cap:
            crypto.add("WEP")
        else:
            crypto.add("OPN")
    print "    %r [%s], %s" % (ssid, bssid,' / '.join(crypto) )
    print "channel: ",channel
    print "rate: ",rate  #rate.decode('ascii') #map( unichr,map(ord,rate))
    return channel

def PacketHandler(pkt) :
  global data_packet, beacon_packet, assoc_packet

  if pkt.haslayer(Dot11) and pkt.haslayer(Dot11Beacon):
		if pkt.type == 0 and pkt.subtype == 8 :
			if pkt.addr2 not in ap_list :
				ap_list.append(pkt.addr2)
				#print "############# Beacon sub=8 ###################################################"
				#wrpcap('filtered.pcap',pkt,append=True)

				capabilities = pkt.sprintf("{Dot11Beacon:%Dot11Beacon.cap%}")
				timestamp = pkt.sprintf("{Dot11Beacon:%Dot11Beacon.timestamp%}")
				#channel = packet_info(pkt)
				#print "channel",channel
				dR1 = rtap.parse(str(pkt))
				print dR1
				channel = dR1['channel'][0]
				print "itamae channel: ",dR1['channel']
				rssi = dR1['antsignal']
				#rssi = parse_packet(pkt)[0]
				mac = pkt[Dot11].addr2

				if mac not in beacon_packet:
					beacon_packet[mac] = [(timestamp,capabilities,channel,rssi)] 
				else:
					beacon_packet[mac].append((timestamp,capabilities,channel,rssi))


  if pkt.haslayer(Dot11) and pkt.haslayer(Dot11ProbeReq):
      wrpcap('filtered.pcap',pkt,append=True)
      print pkt.summary()
      #print pkt.show()
      dR1 = rtap.parse(str(pkt))
      print dR1
      rate = dR1.rate
      rssi = dR1['antsignal']
      mac = pkt[Dot11].addr2 #source-tx address
      destination = pkt[Dot11].addr1
      channel = dR1['channel'][0]
      timestamp = pkt.time
      print "Prob: ",mac,rate,rssi,timestamp
      if mac not in prob_packet:
          prob_packet[mac] = [(destination,rate,rssi,channel,timestamp)]
      else:
          prob_packet[mac].append((destination,rate,rssi,channel,timestamp))


  if pkt.haslayer(Dot11):
  	if pkt.type == 0 and pkt.subtype == 0:
		#print "$$$$$$$$$$$$$ Assoc Request, subtype =0 $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
		#wrpcap('filtered.pcap',pkt,append=True)
		dR1 = rtap.parse(str(pkt))
		print dR1
		rssi = dR1['antsignal']
		#rssi = parse_packet(pkt)[0]
		mac = pkt[Dot11].addr2
		timestamp = pkt.time
		if mac not in assoc_packet:
			assoc_packet[mac] = [(pkt.sprintf("{Dot11AssoReq:%Dot11AssoReq.cap%}"),pkt[Dot11].addr1,rssi,timestamp)]
		else:
			assoc_packet[mac].append((pkt.sprintf("{Dot11AssoReq:%Dot11AssoReq.cap%}"),pkt[Dot11].addr1,rssi,timestamp))

  if pkt.haslayer(Dot11) and pkt.type == 2: # and pkt.addr1 == "4c:49:e3:2b:77:45":
	#print "&&&&&&&&&&&&&&&&&&& DATA PACKETS type=2 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"
	#wrpcap('filtered.pcap',pkt,append=True)
	dR1 = rtap.parse(str(pkt))

	rate = dR1.rate
	rssi = dR1['antsignal']
	mac = pkt[Dot11].addr2 #source-tx address
	destination = pkt[Dot11].addr1
	timestamp = pkt.time
        if mac not in data_packet:
	  if destination != "ff:ff:ff:ff:ff:ff":
	    data_packet[mac] = [(destination,len(pkt),rate,rssi,timestamp)]
	else:
	  if destination != "ff:ff:ff:ff:ff:ff":
            data_packet[mac].append((destination,len(pkt),rate,rssi,timestamp))

'''	
Name       : parse_packet
Description: Parse packet and return rssi 
             To be used under Dot11 layer
Input      : Packet
Output     : Rssi
'''
def parse_packet(pkt):
  radiotap_formats = {"TSFT":"Q", "Flags":"B", "Rate":"B",
  "Channel":"HH", "FHSS":"BB", "dBm_AntSignal":"b", "dBm_AntNoise":"b",
  "Lock_Quality":"H", "TX_Attenuation":"H", "dB_TX_Attenuation":"H",
  "dBm_TX_Power":"b", "Antenna":"B",  "dB_AntSignal":"B",
  "dB_AntNoise":"B", "b14":"H", "b15":"B", "b16":"B", "b17":"B", "b18":"B",
  "b19":"BBB", "b20":"LHBB", "b21":"HBBBBBH", "b22":"B", "b23":"B",
  "b24":"B", "b25":"B", "b26":"B", "b27":"B", "b28":"B", "b29":"B",
  "b30":"B", "Ext":"B"}

  field, val = pkt.getfield_and_val("present")
  names = [field.names[i][0] for i in range(len(field.names)) if (1<<i) & val != 0]
  rate, rssi = None, None
  #check if we measured signal strength
  if "dBm_AntSignal" in names:
      # decode radiotap header
      fmt = "<"
      rssipos = 0	
      for name in names:
        # some fields consist of more than one value
        if name == "dBm_AntSignal":
          # correct for little endian format sign
          rssipos = len(fmt)-1
	fmt = fmt + radiotap_formats[name]
      # unfortunately not all platforms work equally well and on my arm
      # platform notdecoded was padded with a ton of zeros without
      # indicating more fields in pkt.len and/or padding in pkt.pad 
      decoded = struct.unpack(fmt, pkt.notdecoded[:struct.calcsize(fmt)])
      ######################### rate ####################################
      rssi = decoded[rssipos] #,pkt.addr2

  if "Rate" in names:
  	# decode radiotap header
	fmt = "<"
	ratepos = 0
	for name in names:
	  if name == "Rate": 
            ratepos = len(fmt)-1
          fmt = fmt + radiotap_formats[name]
        decoded = struct.unpack(fmt, pkt.notdecoded[:struct.calcsize(fmt)])
	rate = decoded[ratepos]

  return rssi, rate

def tcpdump(file_name):
    #"tcpdump -G 15 -W 1 -w output.pcap -i mon0 ether host 4c:49:e3:2b:77:45"
    args = ['tcpdump', '-G', '5', '-W', '1', '-w', file_name,'-i', 'mon0']#, 'ether', 'host', '4c:49:e3:2b:77:45']
    subprocess.call(args)


if __name__ == '__main__':		
  conn = db_api.create_connection("statistics.db")
  if db_api.vendors_empty(conn):
    execfile("db_api.py")

  channels = [36,40,44]#36,40,44,48
  for channel in channels:
    os.system("iw mon0 set channel " + str(channel))
    #os.system("iw mon0 set freq 5180 80 5210") 
    print "[+] Sniffing on channel "+ str(channel)
    #sniff(iface="mon0", prn=PacketHandler, count=10000, timeout=1, store=0)
    output_file = "capture_"+str(channel)+".pcap"
    tcpdump(output_file)
    time.sleep(1)
    sniff(offline=output_file, prn=PacketHandler, store=0)

    db_api.db_insert_beacon(conn,beacon_packet)
    db_api.db_insert_assocp(conn,assoc_packet)
    db_api.db_insert_datap(conn,data_packet)
    db_api.db_insert_probp(conn, prob_packet)
  conn.close()

