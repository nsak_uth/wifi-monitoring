#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3
import sys
import re

#reload(sys)  
#sys.setdefaultencoding('utf8')
'''
To do: Fix parsing for mac addresses that are given with subnet.
'''


vendors = {}

def parse_vendors():
	count = 0	#counter for the ones note seperated by tab
	not_seperated = []
	global vendors
	with open("vendors.txt","r") as f:
		lines = f.readlines()
		for line in lines:
			if "#" not in line:
				try:
					if len(line.split("\t")) == 2:
						mac = line.split("\t")[0]
						v_id = line.split("\t")[1]
						vendors[mac] = [v_id]

					elif len(line.split("\t")) == 3:
						mac = line.split("\t")[0]
						v_id = line.split("\t")[1]
						v_name = line.split("\t")[2]
						vendors[mac] = [v_id, v_name]
					
					else:
						vendors[mac] = []		
				except Exception as e:
					print e
					print "The ones not seperated by tab "
					not_seperated.append(line)
					count += 1
	
	print "##################################################################################"
	print "##################################################################################"
	print "##################### not seperated by tab########################################"
	#for line in not_seperated:
		#print line

def avg_for(payload_length,rate,rssi):
    """
	Description : Given some lists return their arithmetic mean
	Input       : type list: payload_length, type list: rate, type list: rssi
	Return      : type float: value, type float: value, type float: value
    """
    #remove None rates from list
    rate = [r for r in rate if r != None]  
    payload_length = [payload for payload in payload_length if payload != None]
    rssi = [rs for rs in rssi if rs != None]
    rate_avg = None
    rssi_avg = None
    payload_length_avg = None
    try:
        if rate:
            rate_avg = float(sum(rate)) / max(len(rate), 1)
        if rssi:
            rssi_avg = float(sum(rssi)) / max(len(rssi), 1)	
        if payload_length:
            payload_length_avg = float(sum(payload_length)) / max(len(payload_length), 1)
    except Exception as e:
        print e
    return payload_length_avg, rate_avg, rssi_avg

def search_mac(vendors,mac_address):
    """Description: Searches if mac_address starts with vendors address.
                    For the mac addresses that exist in vendors table in 
                    the form conn.commit()of F0:AC:D7:00:00:00/28 instead of F0:AC:D7: ,
                    special handling is requred. /28 means an address that
                    has the same 28 first bits as the above("F0:AC:D7:00:00:00/28"),
                    its hardware is manufactured from this specific vendor.
                    Each hex digit is represented by 4 bits. Thus 28 bits represent
                    "F0:AC:D7:0".

       if mac.endswith('/36'):
       F0:AC:D7:00:00:00/28
       if mac_address.startswith(mac.lower()[:13]) #for /36 bits   
       if mac_address.startswith(mac.lower()[:10])   # /28 bits
    """
    mac_address = mac_address.lower()

    for mac in vendors:
        if mac.endswith('/36'):    
            if mac_address.startswith(mac.lower()[:13]):
            #print vendors[mac],mac_address,mac
                return vendors[mac][1]
        if mac.endswith('/28'):
            if mac_address.startswith(mac.lower()[:10]):
                return vendors[mac][1]
        if mac_address.startswith(mac.lower()):
            return vendors[mac][1]

    return None


def packet_airtime(conn,first_tp,last_tp):
    """      Calculates packet airtime for data packets.
    Input  : conn,timestamp to begin, timestamp to end
    Output : mac : dictionary with key = mac and value = airtime for each mac
    toDo   : check airtime calculation. Be sure that rate has always this format (*500Kbps)
    """
    macs = []
    mac = {}
    with conn:
        c = conn.cursor()
        c.execute("select id,mac,rate,payload_length from data_packet where timestamp <= ? and timestamp >=? ;",(last_tp,first_tp))
    lines = c.fetchall()
    macs = [line[1] for line in lines]        
    unique_macs = set(macs)

    for m in unique_macs:
        air_time_sum = 0;
        for line in lines:
            if m == line[1]:
                #line[1] mac, line[2] rate, line[3] payload length
                airtime = float(line[3]) / (line[2] * (500 *1000)/8)  #seconds = payload_length/rate make the correct conversion 
                                                                      #payload length is in Bytes and rate in 500Kbps
                air_time_sum += airtime
        mac[m] = air_time_sum / (last_tp - first_tp)
    return mac

def seperate_capabilities(capabilities):
    """     Splitting capabilities string by '+' or '-'.
    Input : string: capabilities
    Output: list of strings: caps 
    """
    caps = re.split('\+|\-',capabilities)
    return caps

def fetch_vendors(conn):
    """ fetch all vendors from database and add
        them to a dictionary.
    Input : conn
    Output: vendors dictionary
    """
    vendors = {}
    with conn:
        c = conn.cursor()
        c.execute("SELECT * FROM vendors")
	lines = c.fetchall()
        for line in lines:
            vendors[line[3]] = [line[2],line[1]]
    return vendors

def db_avg_column(conn,column,table):
    """ Description : Calculate avg() of selected column and table.
        Input       : connection object: connection
	              string: column name
	              string: table name
        Output      : numerical value: sql result  
    """
    query = "SELECT AVG(%s) FROM %s;"%(column,table)
    with conn:
      c = conn.cursor()
      c.execute(query)
      result = c.fetchone()[0]

    return result

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except: #Error as e:
        print("Error trying to connect")
 
    return None

def vendors_empty(conn):
    """ Check if Database is empty.
    :param conn: Connection object
    :return: Boolean value
    """
    with conn:
        c = conn.cursor()
        #c.execute("SELECT count(id) FROM vendors;")
        c.execute("SELECT name FROM  sqlite_master  WHERE type='table' AND  name='vendors'")
        result = c.fetchone()
        if result == None: #32227:
            print "Vendors table does not exist"
            return True
        else:
            return False


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    with conn:
        c = conn.cursor()
        c.execute(create_table_sql)
    #except Exception as e:
    #    print(e)

def db_insert_beacon(conn, beacon_dict):

    insert_beacon_table = """ INSERT INTO beacon_packet(mac,capabilities,vendor,channel,rssi,timestamp) VALUES (?,?,?,?,?,?);"""
    with conn:
        vendors = fetch_vendors(conn)
        c = conn.cursor()
        for mac in beacon_dict:
            for elements in beacon_dict[mac]:
                timestamp = elements[0]
                capabilities = elements[1]
                channel = elements[2]
                rssi = elements[3]
                vendor = search_mac(vendors,mac)
                c.execute(insert_beacon_table,(mac,capabilities,vendor,channel,rssi,timestamp))
        conn.commit()

def db_insert_datap(conn, datap_dict):

    insert_datapacket_table = """ INSERT INTO data_packet(mac,destination,payload_length,rate,rssi,vendor,timestamp) VALUES (?,?,?,?,?,?,?);"""
    with conn:
        vendors = fetch_vendors(conn)
        c = conn.cursor()
        for mac in datap_dict:
            for elements in datap_dict[mac]:
                destination = elements[0]
                payload_length = elements[1]
                rate = elements[2]
                rssi = elements[3]
                timestamp = elements[4]
                vendor = search_mac(vendors,mac)
                c.execute(insert_datapacket_table,(mac,destination,payload_length,rate,rssi,vendor,timestamp))
        conn.commit()

def db_insert_probp(conn, probp_dict):

    insert_probpacket_table = """ INSERT INTO prob_packet(mac,destination,rate,rssi,vendor,channel,timestamp) VALUES (?,?,?,?,?,?,?);"""
    with conn:
        vendors = fetch_vendors(conn)
        c = conn.cursor()
        for mac in probp_dict:
            for elements in probp_dict[mac]:
                destination = elements[0]
                rate = elements[1]
                rssi = elements[2]
                channel = elements[3]
                timestamp = elements[4]
                vendor = search_mac(vendors,mac)
                c.execute(insert_probpacket_table,(mac,destination,rate,rssi,vendor,channel,timestamp))
        conn.commit()  

def db_insert_assocp(conn,assoc_req_dict):
    insert_assocp_table = """ INSERT INTO association_packet(mac,capabilities,ap_to_connect,rssi,timestamp) VALUES (?,?,?,?,?);"""

    with conn:
        #vendors = fetch_vendors()
        c = conn.cursor()
        for mac in assoc_req_dict:
            for elements in assoc_req_dict[mac]:
                capabilities = elements[0]
                ap_to_connect = elements[1]
                rssi = elements[2]
                timestamp = elements[3]
                c.execute(insert_assocp_table,(mac,capabilities,ap_to_connect,rssi,timestamp))
        conn.commit()

def database_init():
	"""
	Description : Create database "statistics.db" and create vendors,
		      association_packet and data_packet tables. Needs to run one time. 
	Input       : 
	Output      : 
	"""
	database = "statistics.db"
	
	sql_create_data_packet_avg_table = """ CREATE TABLE IF NOT EXISTS data_p_stats (id integer PRIMARY KEY,
										mac text NOT NULL,
										destination text,
										payload_length_avg real,
										rate_avg  real,rssi_avg real,vendor text,
										timestamp integer, packets_send integer, throughput real);""" 
	#sto timestamp vale 2 ena to xekinima kai ena to telos. twra logka krataw to telos
	sql_create_vendors_table = """ CREATE TABLE IF NOT EXISTS vendors (
                                        id integer PRIMARY KEY,
                                        vendor_id text NOT NULL,
                                        vendor_name text,
                                        mac_address text
                                    ); """
 
	sql_create_assocpacket_table = """ CREATE TABLE IF NOT EXISTS association_packet (
				    				id integer PRIMARY KEY,
                                    mac text NOT NULL,
									capabilities text NOT NULL,
                                    ap_to_connect text NOT NULL,rssi integer,timestamp integer
                                    
                                );"""

	sql_create_data_packet_table = """ CREATE TABLE IF NOT EXISTS data_packet (id integer PRIMARY KEY,
										mac text NOT NULL,
										destination text,
										payload_length integer,
										rate integer,rssi integer,vendor text,
										timestamp integer);""" 
	sql_create_beacons_packet_table =  """ CREATE TABLE IF NOT EXISTS beacon_packet (
										id integer PRIMARY KEY,
										mac text NOT NULL,
										capabilities text NOT NULL,
										vendor text,channel integer,rssi integer,
										timestamp integer); """

	sql_create_probr_packet_table =  """ CREATE TABLE IF NOT EXISTS prob_packet (
										id integer PRIMARY KEY,
										mac text NOT NULL,
										destination text NOT NULL,
										rate integer,
										vendor text,channel integer,rssi integer,
										timestamp integer); """



	#FOREIGN KEY (project_id) REFERENCES projects (id)
	# create a database connection
	conn = create_connection(database)
	if conn is not None:
        # create projects table
		create_table(conn, sql_create_vendors_table)
        # create tasks table
		create_table(conn, sql_create_assocpacket_table)
		create_table(conn,sql_create_data_packet_table)
		create_table(conn,sql_create_beacons_packet_table)
		create_table(conn, sql_create_data_packet_avg_table)
		create_table(conn, sql_create_probr_packet_table)
	else:
		print("Error! cannot create the database connection.")
	return conn


def populate_vendors_table(conn):
	"""
	Description	: Insert into table: vendors ,all the vendor's info. 
	Input		: Connection to db
	Output		: 
	"""
	with conn:
	#insert vendors information in vendors table
		c = conn.cursor()
		init_vendors_table = """ INSERT INTO vendors(vendor_id,vendor_name,mac_address) VALUES (?,?,?);"""
		for mac in vendors:
			if len (list(vendors[mac])) > 0:
				if len (list(vendors[mac])) > 1:
					c.execute(init_vendors_table,(str(list(vendors[mac])[0]).decode('utf-8'),str(list(vendors[mac])[1]).decode('utf-8'),mac))
				else:
					c.execute(init_vendors_table,(str(list(vendors[mac])[0]).decode('utf-8'),"",mac))


def db_fetch_packet_data(conn,p_table="data_packet"):
    """
    Descriptin : Fetch all rows from data_packet table
    Input      : connection object: conn
    Return     : list of lists: each list represents a 
                 row from data_packet. [[id,mac,dest,..],[, , ,..],[]... 
    """
    query = "SELECT * FROM %s" % p_table
    with conn:
        c = conn.cursor()
        c.execute(query)
        data = c.fetchall()
    return data

def remove_old_entries(conn,table_name):
   """
   Descriptin : Remove data packet entries from data_packet
                table after they have been backed up to the 
                remote server.
   Input      : connection object: conn
   Return     :  
   """
   delete_entries = """DELETE FROM %s  """ %table_name
   with conn:
      c = conn.cursor()
      #commit = raw_input("Are you sure you want to delete all entries of data packet? [y]: ")
      #if commit == "y":
      c.execute(delete_entries)
      conn.commit()
      print c.fetchall()

def db_get_sessions(conn):
    """ 
    Description  : Find all sessions of the last run. Checks source destination 
		           addresses and finds all the occurances of the same source
				   destination and saves them as one session.
    Input        : connection object: connection
    Return       : a list of sessions   
    """
    query = "SELECT * FROM data_packet;"
    sessions = []

    with conn:
        c = conn.cursor()
        c.execute(query)
        result = c.fetchall()

    for row in result:
        source_dest = (row[1],row[2])
        session = []
		
        for row in result:
            if source_dest == (row[1],row[2]) or source_dest == (row[2],row[1]) and row not in session:  #des to auto sto or koitaw ki an source destination = destination source. isxuei auto gia ta session?
                session.append(row)
        if session not in sessions:		
            sessions.append(session)
		
    return sessions

def  db_insert_sessions(conn,sessions):
    """ 
    Description  : insert into data_p_stats table the sessions
                             found and their statistics.
    Input        : connection object: connection, list: sessions
    Return       :    
    """    
    insert_datapacket_table = """ INSERT INTO data_p_stats(mac,destination,payload_length_avg,rate_avg,rssi_avg,timestamp,packets_send,throughput) VALUES (?,?,?,?,?,?,?,?);"""
   
    with conn:
        c = conn.cursor()
        for session in sessions:
            payload_lengths = []
            rates = []
            rssis = []
            timestamps = []
            for packet in session:
                mac = packet[1]
                destination = packet[2]  #auta xanadesta. ousiastika edw pernw thn teleutaia emfainish sth lista tou kathe session ths mac kai destination. Mporei na mhn einai se oles tis emfaniseis idia
                payload_lengths.append( packet[3] )
                rates.append( packet[4] )
                rssis.append( packet[5] )
                timestamp = packet[7]
                timestamps.append(long(timestamp))           
            packets_send = len(session)				
            payload_length_avg, rate_avg, rssi_avg = avg_for(payload_lengths,rates,rssis)

            #calculate throughput
            if max(timestamps)-min(timestamps) == 0:
                throughput = sum(payload_lengths)
            else:
                throughput = sum(payload_lengths)/(max(timestamps)-min(timestamps))

            #check if session already exists in data_p_stats table
            c.execute('SELECT EXISTS(SELECT 1 FROM data_p_stats WHERE timestamp=? LIMIT 1);',(timestamp,))
            exists = c.fetchone()
            if exists[0] == 0:			
                c.execute( insert_datapacket_table, (mac, destination, payload_length_avg, rate_avg, rssi_avg, timestamp, packets_send, throughput))   
                

                      
    pass
if __name__ == '__main__':
	parse_vendors()
	conn = database_init()
	populate_vendors_table(conn)
	


